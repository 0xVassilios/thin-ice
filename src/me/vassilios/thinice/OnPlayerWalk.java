package me.vassilios.thinice;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class OnPlayerWalk implements Listener {
	private int radius = 4;

	@EventHandler()
	public void onPlayerWalk(PlayerMoveEvent event) {
		if (event.getPlayer() instanceof Player && hasPlayerMoved(event)) {
			Block playerBlock = event.getTo().getBlock().getRelative(BlockFace.DOWN);
			
			if (playerBlock.getType().equals(Material.ICE)) {
				if (isIceBreakable(playerBlock)) {
					breakRadius(playerBlock.getLocation());
				}
			}
		}
	}
	
	public Boolean hasPlayerMoved(PlayerMoveEvent event) {
		if (event.getFrom().getBlockX() == event.getTo().getBlockX() && event.getFrom().getBlockY() == event.getTo().getBlockY() && event.getFrom().getBlockZ() == event.getTo().getBlockZ()) {
			return false;
		}
		return true;
	}
	
	public Boolean isIceBreakable(Block block) {
		
		for (int distance = 1; distance <= 4; distance++) {
			if (!block.getRelative(BlockFace.DOWN, distance).getType().equals(Material.WATER)) {
				return false;
			}
		}
		
		return true;
	}
	
	public void breakRadius(Location location) {
		
		for (int x = location.getBlockX() - this.radius; x <= location.getBlockX() + radius; x++) {
			for (int z = location.getBlockZ() - this.radius; z <= location.getBlockZ() + radius; z++) {
				Block block = location.getWorld().getBlockAt(x, (int) location.getY(), z);
				
				if (block.getType().equals(Material.ICE)) {
					block.breakNaturally();
				}
			}
		}
	}
	
}
